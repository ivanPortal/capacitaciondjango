from django.conf.urls import url
from core.user import views


urlpatterns = [

    url('create', views.CreateUser.as_view(), name='createUser'),

]