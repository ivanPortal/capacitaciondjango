asgiref==3.3.1
Django==3.0
django-cors-headers==3.5.0
djangorestframework==3.11.1
psycopg2==2.8.6
pytz==2020.5
sqlparse==0.4.1
