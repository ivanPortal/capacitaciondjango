# CapacitaciónDjango

# Comando para iniciar un proyecto

django-admin startproject <nombre_de_proyecto>

# Iniciar una aplicación

django-admin startapp <nombre_app>

# Comandos para hacer el build de Docker y Docker-Compose

docker build .

docker-compose build

# Comando para levantar el contenedor

docker-compose up
